"""Utilities."""

from __future__ import annotations

__date__ = "2024/08/07 17:24:45 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2017 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"


def fonds_unify(name: str) -> str:
    """Unitfy name of requeste fonds."""
    if name in {
        "Swisscanto (LU) Portfolio Fund Green Invest Equity A",
        "Swisscanto (LU) Equity Fund Sustainable AA EUR",
    }:
        return "Swisscanto (LU) Portfolio Fund Green Invest Equity AA"
    return name
