"""Get portfolio data from SwissAcconto email."""

from __future__ import annotations

from collections.abc import Buffer
import datetime
import email
import mailbox
from mailbox import MHMessage
import re
from typing import IO, TYPE_CHECKING, Any

from rich.console import Console

from . import db_def
from .utils import fonds_unify

if TYPE_CHECKING:
    from pathlib import Path
    import sqlite3 as lite


__date__ = "2024/12/14 21:47:41 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2012, 2021, 2024 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"

CONSOLE = Console()


@db_def.with_connection
def init_table(cur: lite.Cursor) -> None:
    """Initialize database if it does not exist."""
    tables = cur.execute(
        """
SELECT name FROM sqlite_master
WHERE type='table'
ORDER BY name;""",
    ).fetchall()

    if not tables or ("MSG_IDS",) not in tables:
        cur.execute(
            """
CREATE TABLE MSG_IDS(
  ID STRING(80) NOT NULL UNIQUE)""",
        )


init_table()

SUBJECT_MATCH = re.compile(
    r"""(?:Swisscanto-Notification) |
                           (?:mySwisscanto\ Portfolio)""",
    re.MULTILINE | re.VERBOSE | re.UNICODE,
)

CONTENT = (
    r"""(?:
           (?P<var1>Der\ Kurs\ per\ #
             (?P<TimeStamp>\d{2}\.\d{2}\.\d{2})\ des\ #
             '(?P<fonds1>.+)'.+EUR\ #
             (?P<Value1>\d+\.\d{2})\.
           )
         )
         |
         (?:
           (?P<var2>
             (?P<fonds2>.+)\ \ EUR\s+
             (?P<Value2>\d+\.\d{2})
           )
         )""",
    re.VERBOSE | re.UNICODE,
)

DATA_MATCH = re.compile(*CONTENT)


class SADBAccess(db_def.SADBAccess):
    """DB access wrapper."""

    @staticmethod
    @db_def.with_connection
    def add_msg_id(cur: lite.Cursor, message_id: str) -> None:
        """Add new message id to database."""
        cur.execute(
            """
INSERT INTO MSG_IDS(ID) VALUES ( ? )""",
            [message_id],
        )


class SAMailRead:
    """Read legacy mail from SwissAcconto."""

    def __init__(self, mailsource: Path) -> None:
        """Initialize class instance."""
        self.mbox = mailbox.MH(mailsource, factory=self.msgfactory, create=False)

    def msgfactory(self, file_p: IO[Any]) -> MHMessage:
        """Fault tolerant message reader."""
        try:
            return MHMessage(file_p)
        except email.errors.MessageParseError:
            # Don't return None since that will stop the mailbox
            # iterator
            return MHMessage("")

    @staticmethod
    @db_def.with_connection
    def get_msg_ids(cur: lite.Cursor) -> list[str]:
        """Return id for message."""
        return cur.execute("SELECT ID from MSG_IDS").fetchall()

    def __call__(self) -> None:
        """Executre data processing."""
        db_conn = SADBAccess()

        known_ids = [i[0] for i in SAMailRead.get_msg_ids()]
        for msg in self.mbox:
            if msg["message-id"] in known_ids:
                continue

            subject = SUBJECT_MATCH.match(
                " ".join([i[0] for i in email.header.decode_header(msg["subject"])]),
            )
            if subject:
                res = SAMailRead.parse_mail(db_conn, msg)
                if res is not None:
                    known_ids.append(res)

            else:
                CONSOLE.print(f'Not parsed message with subject:\n!{msg["subject"]}!')
                CONSOLE.print(
                    type(msg["subject"]),
                    [i[0] for i in email.header.decode_header(msg["subject"])],
                )

    @staticmethod
    def parse_mail(db_conn: SADBAccess, msg: email.message.Message) -> str | None:
        """Extract information from email."""
        char_set = msg.get_charset()
        payload: (
            email.message.Message[str, str]
            | str
            | list[email.message.Message[str, str] | str]
            | Any
        )
        if char_set is not None:
            if not isinstance(char_set, str):
                raise TypeError
            p = msg.get_payload()
            if not isinstance(p, Buffer):
                raise TypeError
            payload = str(p, char_set)
        else:
            payload = msg.get_payload()
        if not isinstance(payload, str):
            raise TypeError
        message_cont = DATA_MATCH.search(payload)

        if message_cont is None:
            return None

        if message_cont.group("var1"):
            date = (
                datetime.datetime.strptime(message_cont.group("TimeStamp"), "%d.%m.%y")
                .astimezone()
                .date()
            )

            value = float(message_cont.group("Value1"))

            name = fonds_unify(message_cont.group("fonds1"))
            db_conn.add_datapoint(name, date, value)
            db_conn.add_msg_id(msg["message-id"])
            return msg["message-id"]

        if message_cont.group("var2"):
            dtup = email.utils.parsedate(msg["Delivery-date"])
            if not isinstance(dtup, tuple):
                raise ValueError
            date = datetime.date(*dtup[:3])

            value = float(message_cont.group("Value2"))

            name = fonds_unify(message_cont.group("fonds2"))
            db_conn.add_datapoint(name, date, value)
            db_conn.add_msg_id(msg["message-id"])
            return msg["message-id"]

        CONSOLE.print(payload)
        CONSOLE.print(CONTENT)
        CONSOLE.print(message_cont.groupdict())
        if True:
            emsg = "Error: No Volume data found in email"
            raise ValueError(emsg)
        CONSOLE.print("ignored")
        return None
