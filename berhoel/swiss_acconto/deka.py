"""Read fontds data from DEKA."""

from __future__ import annotations

import datetime
import os
import re

from lxml.html import fromstring
from rich.console import Console
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait

from .db_def import SADBAccess
from .utils import fonds_unify

__date__ = "2024/12/14 21:49:04 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2017, 2021 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"

CONSOLE = Console()


class Selenium:
    """Selenium wrapper."""

    def __init__(self) -> None:
        """Initialize class instance.

        Virtually private constructor.
        """
        options = Options()
        options.add_argument("--headless")
        if os.name == "nt":
            options.add_argument("--disable-gpu")  # Last I checked this was necessary.
        firefox_profile = FirefoxProfile()
        firefox_profile.set_preference(key="javascript.enabled", value=True)
        options.profile = firefox_profile
        self.driver = webdriver.Firefox(options=options)
        self.driver.get("https://www.deka.de/")

    def close(self) -> None:
        """Close webbrowser connection."""
        self.driver.close()
        self.driver.quit()


class GetValuesFromDeka:
    """Retrieve fonds information from DEKA website."""

    regexp_value = re.compile(
        r"""(?P<eur>[0-9]+),
(?P<cent>[0-9]{2})""",
        re.VERBOSE | re.UNICODE | re.DOTALL,
    )
    regexp_date = re.compile(
        r"""
(?P<day>[0-3]?[0-9])\.
(?P<month>[01]?[0-9])\.
(?P<year>20[0-9][0-9])""",
        re.VERBOSE | re.UNICODE,
    )

    def __init__(self, url: str) -> None:
        """Initialize class instance."""
        self.selenium = Selenium()
        self.selenium.driver.get(url)
        self.selenium.driver.execute_script(
            'document.getElementsByTagName("html")[0].style.scrollBehavior = "auto"',
        )
        self.doc = fromstring(self.selenium.driver.page_source)

    def get_date(self) -> datetime.date:
        """Extract date information from website."""
        xpath_date = (
            "/html/body/div[2]/main/div[2]/div/div/div/div[2]/div/div/div/div[1]/"
            "div[4]/p[2]/text()"
        )
        xpath_date = '//div[@class="date-box"]/p[2]/text()'
        # 'Aktuelle Fondsdaten vom 19.10.2017'
        rdata = self.doc.xpath(xpath_date)
        if not isinstance(rdata, (list, tuple)):
            raise TypeError
        data = rdata[0]
        if not isinstance(data, str):
            raise TypeError
        match = self.regexp_date.match(data.strip())
        if match is None:
            raise ValueError
        res = match.groupdict()
        return datetime.date(int(res["year"]), int(res["month"]), int(res["day"]))

    def get_value(self) -> float:
        """Extract value information from website."""
        # '\n                        '
        # '\t\t                            146,68 EUR'
        # '\n\t                                                '
        xpath_value = '//div[@class="price-box-one--inner"]/h2'
        wait = WebDriverWait(self.selenium.driver, 10)
        data = wait.until(ec.element_to_be_clickable((By.XPATH, xpath_value))).text
        match = self.regexp_value.match(data)
        if match is None:
            raise ValueError
        res = match.groupdict()
        return float(res["eur"]) + float(res["cent"]) / 100.0

    def get_name(self) -> str:
        """Extract fonds name information from website."""
        xpath_name = '//*[@id="anker12847227"]/div[1]/div[1]/div/div/h1/text()'
        rdata = self.doc.xpath(xpath_name)
        if not isinstance(rdata, (list, tuple)):
            raise TypeError
        data = rdata[0]
        if not isinstance(data, str):
            raise TypeError
        return fonds_unify(data)

    def close(self) -> None:
        """Close selenium connection."""
        self.selenium.close()


def read_data(url: str) -> None:
    """Process data from current website."""
    prog = GetValuesFromDeka(url)
    date = prog.get_date()
    value = prog.get_value()
    name = prog.get_name()
    prog.close()

    db_conn = SADBAccess()

    db_conn.add_datapoint(name, date, value)


def main() -> None:
    """Test data retrieval."""
    # Wichtiger Hinweis!
    # Dieser Investmentfonds wird am 13.08.2018 umbenannt in Swisscanto (LU)
    # Equity Fund Sustainable AA EUR (ISIN: LU0208341965).
    prog = GetValuesFromDeka(
        "https://www.deka.de/privatkunden/fondsprofil?id=LU0161535835",
    )

    CONSOLE.print(prog.get_date())
    CONSOLE.print(prog.get_value())
    CONSOLE.print(prog.get_name())
    prog.close()


if __name__ == "__main__":
    main()
