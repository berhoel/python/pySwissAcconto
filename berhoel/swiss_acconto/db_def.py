"""Database definitions."""

from __future__ import annotations

from pathlib import Path
import sqlite3 as lite
from typing import TYPE_CHECKING, Callable

if TYPE_CHECKING:
    from collections.abc import Generator
    import datetime

__date__ = "2024/12/14 21:47:22 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2017, 2021 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"

# Target directory for database.
DATA_DIR = Path.home() / ".config" / "SwissAcconto"

# Database file name.
DB_FILE = DATA_DIR / "data.db"


def with_connection(f: Callable) -> Callable:
    """Wrap database connection."""

    def with_connection_(*args: object, **kwargs: object) -> list[str]:
        # or use a pool, or a factory function...

        rv = None
        with lite.connect(
            str(DB_FILE),
            detect_types=lite.PARSE_DECLTYPES | lite.PARSE_COLNAMES,
        ) as con:
            cur = con.cursor()

            try:
                rv = f(cur, *args, **kwargs)
            except Exception:
                con.rollback()
                raise
            else:
                con.commit()  # or maybe not

        return rv

    return with_connection_


# Initialize database.
@with_connection
def init_db(cur: lite.Cursor) -> None:
    """Initialize database."""
    tables = cur.execute(
        "SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;",
    ).fetchall()

    if not tables or ("SA_DATA",) not in tables:
        cur.execute(
            """
CREATE TABLE SA_DATA(
  Date DATE NOT NULL PRIMARY KEY,
  Price REAL NOT NULL,
  Fonds_ID INTEGER NOT NULL)""",
        )

    if not tables or ("FONDS_NAMES",) not in tables:
        cur.execute(
            """
CREATE TABLE FONDS_NAMES(
  ID INTEGER NOT NULL PRIMARY KEY,
  Name VARCHAR(40) NOT NULL)""",
        )


# Create DATA_DIR if it does not exist.
if not DATA_DIR.exists():
    DATA_DIR.mkdir()

# Ensure DATA_DIR is a directory.
if not DATA_DIR.is_dir():
    msg = f"{DATA_DIR} is not a directory."
    raise SystemExit(msg)

init_db()

# Make sure database file does exist.
if not DB_FILE.is_file():
    msg = f"{DB_FILE} is not a file."
    raise SystemExit(msg)


class SADBAccess:
    """Access databse to store previous results."""

    @staticmethod
    def _get_fonds_id(cur: lite.Cursor, f_name: str) -> str:
        """Return unified ID for font."""
        res = cur.execute(
            """
SELECT ID
FROM FONDS_NAMES
WHERE Name=?""",
            [f_name],
        ).fetchone()
        if not res:
            cur.execute(
                """
INSERT INTO FONDS_NAMES(Name) VALUES ( ? )""",
                [f_name],
            )
            res = cur.execute(
                """
SELECT ID from FONDS_NAMES where Name=?""",
                [f_name],
            ).fetchone()
        return res[0]

    @staticmethod
    @with_connection
    def add_datapoint(
        cur: lite.Cursor,
        f_name: str,
        date: datetime.datetime,
        value: float,
    ) -> None:
        """Add data to database."""
        res = SADBAccess._get_fonds_id(cur, f_name)

        entry = cur.execute(
            """
SELECT Date
FROM SA_DATA
WHERE Fonds_ID=? AND Date=?""",
            (res, date),
        ).fetchone()
        if not entry:
            cur.execute(
                """
INSERT INTO SA_DATA
VALUES (?, ?, ?)""",
                (date, value, res),
            )

    @staticmethod
    @with_connection
    def read_data(cur: lite.Cursor) -> Generator:
        """Read data from database."""
        fonds = cur.execute("SELECT ID, Name from FONDS_NAMES").fetchall()
        for _id, name in fonds:
            raw = cur.execute(
                """
SELECT Date, Price
FROM SA_DATA
WHERE Fonds_ID=?
ORDER by Date""",
                (_id,),
            ).fetchall()
            data = [[date, price] for date, price in raw]
            yield (name, data)
