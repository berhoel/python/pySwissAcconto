"""Analysis of SwissAcconto Mails."""

from __future__ import annotations

import datetime
from pathlib import Path

import matplotlib as mpl
from matplotlib import gridspec
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np

from . import db_def, deka, samail

__date__ = "2024/08/07 18:32:20 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2007 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"
__version__ = __import__("importlib.metadata", fromlist=["version"]).version(
    "SwissAcconto",
)

FOCUSDAYS = 90

# Is processing of emails required?
PROCESS_MAIL = False

MAILSOURCE = (Path().home() / "Mail" / "Swisscanto.LU0161535835").glob("*")

URLS = ("https://www.deka.de/privatkunden/fondsprofil?id=LU0161535835",)


def get_deka() -> None:
    """Get information from DEKA website."""
    for url in URLS:
        deka.read_data(url)


def get_from_mail() -> None:
    """Get Information from old SwissAcconto status emails."""
    for mailsource in MAILSOURCE:
        data = samail.SAMailRead(mailsource)
        data()


def main() -> None:  # noqa:PLR0915
    """Process all data."""
    mpl.use("GTK4Cairo")

    get_deka()
    if PROCESS_MAIL:
        get_from_mail()

    source = db_def.SADBAccess()

    for fonds, data_ in source.read_data():
        data = np.array(data_)
        date_fmt = mdates.DateFormatter("%Y-%m-%d")

        figure = plt.figure()
        grid_space = gridspec.GridSpec(2, 1)

        dates = data[:, 0]
        f_years = np.array(
            [
                d.year
                + d.timetuple().tm_yday
                / float(datetime.date(d.year, 12, 31).timetuple().tm_yday)
                for d in dates
            ],
        )
        start_date = data[0, 0]

        days_of_year = float(datetime.date(start_date.year, 12, 31).timetuple().tm_yday)

        f_years_start = start_date.year + start_date.timetuple().tm_yday / days_of_year
        f_years -= f_years_start

        min_point = np.argmin(data[:, 1])
        offs_years = np.array(
            [
                d.year
                + d.timetuple().tm_yday
                / float(datetime.date(d.year, 12, 31).timetuple().tm_yday)
                for d in dates[min_point:]
            ],
        )
        offs_start_date = data[min_point, 0]
        offs_years_start = float(
            datetime.date(offs_start_date.year, 12, 31).timetuple().tm_yday,
        )
        offs_f_years_start = (
            offs_start_date.year
            + offs_start_date.timetuple().tm_yday / offs_years_start
        )
        offs_years -= offs_f_years_start

        axis_a = figure.add_subplot(grid_space[0])
        axis_a.set_title(fonds)
        axis_a.grid(visible=True)
        axis_a.xaxis.set_major_formatter(date_fmt)
        axis_a.plot_date(dates, data[:, 1], fmt="-", label="data")
        axis_a.plot_date(
            dates,
            1.05**f_years * data[0, 1],
            fmt="-",
            label="5% seit Start",
        )
        # , label="1.5% seit Start")
        axis_a.plot_date(dates, 1.015**f_years * data[0, 1], fmt="-")
        axis_a.plot_date(
            dates,
            np.zeros(len(f_years)) + data[0, 1],
            fmt="-",
            label="Kaufpreis",
        )
        axis_a.plot_date(
            dates[min_point:],
            1.05**offs_years * data[min_point, 1],
            fmt="-",
            label="5% seit Tiefpunkt",
        )
        axis_a.plot_date(
            dates[min_point:],
            1.10**offs_years * data[min_point, 1],
            fmt="-",
        )
        axis_a.plot_date(
            dates[min_point:],
            1.15**offs_years * data[min_point, 1],
            fmt="-",
        )
        axis_a.plot_date(
            dates[min_point:],
            1.20**offs_years * data[min_point, 1],
            fmt="-",
        )
        axis_a.legend(shadow=True, loc=(0.01, 0.01))
        for t in axis_a.get_xmajorticklabels():
            t.set_rotation(30)
        border = 0.02 * (dates[-1] - dates[0])

        axis_a.axis((dates[0] - border, dates[-1] + border, 0.0, None))  # type: ignore[arg-type]

        axis_b = figure.add_subplot(grid_space[1])
        axis_b.grid(visible=True)
        axis_b.xaxis.set_major_formatter(date_fmt)
        axis_b.plot_date(
            dates[-FOCUSDAYS - 1 :],
            data[-FOCUSDAYS - 1 :, 1],
            fmt="+-",
            label="data",
        )
        axis_b.plot_date(
            dates[-FOCUSDAYS - 1 :],
            (1.05**f_years * data[0, 1])[-FOCUSDAYS - 1 :],
            fmt="-",
            label="5% seit Start",
        )
        axis_b.plot_date(
            dates[-FOCUSDAYS - 1 :],
            np.zeros(FOCUSDAYS + 1) + data[0, 1],
            fmt="-",
            label="Kaufpreis",
        )

        relev_years = offs_years[-FOCUSDAYS - 1 :]
        axis_b.plot_date(
            dates[-FOCUSDAYS - 1 :],
            1.05**relev_years * data[min_point, 1],
            fmt="-",
            label="5% seit Tiefpunkt",
        )
        axis_b.plot_date(
            dates[-FOCUSDAYS - 1 :],
            1.10**relev_years * data[min_point, 1],
            fmt="-",
            label="10% seit Tiefpunkt",
        )
        axis_b.plot_date(
            dates[-FOCUSDAYS - 1 :],
            1.15**relev_years * data[min_point, 1],
            fmt="-",
            label="15% seit Tiefpunkt",
        )
        axis_b.plot_date(
            dates[-FOCUSDAYS - 1 :],
            1.20**relev_years * data[min_point, 1],
            fmt="-",
            label="20% seit Tiefpunkt",
        )
        axis_b.plot_date(
            dates[-FOCUSDAYS - 1 :],
            1.25**relev_years * data[min_point, 1],
            fmt="-",
            label="25% seit Tiefpunkt",
        )
        axis_b.legend(shadow=True, loc=(0.01, 0.3))
        for t in axis_b.get_xmajorticklabels():
            t.set_rotation(30)
        startdate = datetime.datetime.now(
            tz=datetime.timezone.utc,
        ) - datetime.timedelta(days=FOCUSDAYS)
        enddate = datetime.datetime.now(tz=datetime.timezone.utc) + datetime.timedelta(
            days=0.5,
        )
        axis_b.axis((startdate, enddate, None, None))  # type: ignore[arg-type]

        figure.subplots_adjust(
            left=0.05,
            right=0.95,
            bottom=0.06,
            top=0.95,
            hspace=0.14,
        )

        plt.show()


if __name__ == "__main__":
    main()
