"""Testing."""

import pytest

from berhoel.swiss_acconto import utils

__date__ = "2024/08/09 16:35:05 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2022 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"


LU_NAME_1 = "Swisscanto (LU) Portfolio Fund Green Invest Equity A"
LU_NAME_2 = "Swisscanto (LU) Equity Fund Sustainable AA EUR"
LU_NAME_UNIFIED = "Swisscanto (LU) Portfolio Fund Green Invest Equity AA"


@pytest.mark.parametrize("fonds", [LU_NAME_1, LU_NAME_2])
def test_fonds_unify(fonds: str):
    assert utils.fonds_unify(fonds) == LU_NAME_UNIFIED
