#! /usr/bin/env python
"""Testing the code."""

__date__ = "2021/12/11 16:17:59 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2015 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"

# Local Variables:
# mode: python
# compile-command: "poetry run tox"
# time-stamp-pattern: "30/__date__ = \"%:y/%02m/%02d %02H:%02M:%02S %u\""
# End:
