..
  Task: Analysis of SwissAcconto fonds information.

  :Authors:
    - `Berthold Höllmann <berhoel@gmail.com>`__
  :Organization: Berthold Höllmann
  :Date: 2018-09-30
  :datestamp: %Y-%m-%d
  :Copyright: Copyright © 2018 by Berthold Höllmann

=============================================
 Analysis of SwissAcconto fonds information.
=============================================

Reads emails from SwissAcconto fonds and displays development.

..
  Local Variables:
  mode: rst
  compile-command: "make html"
  coding: utf-8
  time-stamp-pattern: "20/^  :Date: %:y-%02m-%02d$"
  End:
